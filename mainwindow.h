#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"

#include <QMainWindow>
#include <QFileInfoList>

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_sourceTB_clicked();
    void on_destinationTB_clicked();
    void on_searchPB_clicked();
    void on_updatePB_clicked();

private:
    QFileInfoList m_sourceFiles;
    QFileInfoList m_destinationFiles;
};

#endif // MAINWINDOW_H
