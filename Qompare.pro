#-------------------------------------------------
#
# Project created by QtCreator 2011-07-17T11:14:42
#
#-------------------------------------------------

QT       += core gui

TARGET = Qompare
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
