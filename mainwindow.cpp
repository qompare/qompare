#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QFileInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);
}

MainWindow::~MainWindow()
{
}

void MainWindow::on_sourceTB_clicked()
{
    QString sourceDirPath = QFileDialog::getExistingDirectory(this, trUtf8("Choose source folder"), QDir::homePath());
    if (sourceDirPath.isEmpty()) return;

    sourceTW->setRowCount(0);

    sourceL->setText(sourceDirPath);
    m_sourceFiles = QDir(sourceDirPath).entryInfoList(QDir::Files);

    const int nbrOfSourceFiles = m_sourceFiles.size();
    sourceTW->setRowCount(nbrOfSourceFiles);
    for (int i = 0; i < nbrOfSourceFiles; ++i)
    {
        sourceTW->setItem(i, 0, new QTableWidgetItem(m_sourceFiles[i].fileName()));
        sourceTW->setItem(i, 1, new QTableWidgetItem(QString::number(m_sourceFiles[i].size())));
    }
}

void MainWindow::on_destinationTB_clicked()
{
    QString destinationDirPath = QFileDialog::getExistingDirectory(this, trUtf8("Choose destination folder"), QDir::homePath());
    if (destinationDirPath.isEmpty()) return;

    destinationTW->setRowCount(0);

    destinationL->setText(destinationDirPath);
    m_destinationFiles = QDir(destinationDirPath).entryInfoList(QDir::Files);

    const int nbrOfDestinationFiles = m_destinationFiles.size();
    destinationTW->setRowCount(nbrOfDestinationFiles);
    for (int i = 0; i < nbrOfDestinationFiles; ++i)
    {
        destinationTW->setItem(i, 0, new QTableWidgetItem(m_destinationFiles[i].fileName()));
        destinationTW->setItem(i, 1, new QTableWidgetItem(QString::number(m_destinationFiles[i].size())));
    }
}

void MainWindow::on_searchPB_clicked()
{
    QString currentDestinationName;
    QString currentDestinationSize;
    const int nbrOfSourceFiles = m_sourceFiles.size();
    const int nbrOfDestinationFiles = m_destinationFiles.size();

    for (int i = 0; i < nbrOfDestinationFiles; ++i)
    {
        currentDestinationName = destinationTW->item(i, 0)->text();
        currentDestinationSize = destinationTW->item(i, 1)->text();
        for (int j = 0; j < nbrOfSourceFiles; ++j)
        {
            if (sourceTW->item(j, 1)->text() == currentDestinationSize)
            {
                if (sourceTW->item(j, 0)->text() != currentDestinationName)
                {
                    destinationTW->item(i, 0)->setText(sourceTW->item(j, 0)->text());
                    destinationTW->setItem(i, 2, new QTableWidgetItem(";" + QString::number(j)));
                }
                else
                    destinationTW->setItem(i, 2, new QTableWidgetItem(QString::number(j)));

                break;
            }
        }
    }
}

void MainWindow::on_updatePB_clicked()
{
    const int nbrOfDestinationFiles = m_destinationFiles.size();
    QDir destinationDirectory(destinationL->text());
    for (int i = 0; i < nbrOfDestinationFiles; ++i)
    {
        if (destinationTW->item(i, 2) && destinationTW->item(i, 2)->text().startsWith(";"))
            destinationDirectory.rename(m_destinationFiles[i].fileName(), destinationTW->item(i, 0)->text());
    }
}
